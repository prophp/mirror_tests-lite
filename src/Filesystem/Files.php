<?php

namespace ProPhp\Mirror\TestsLite\Filesystem;

class Files
{
    // @todo Use Params(): excludedDirPatterns(), includedDirPatterns(), exludedFilenamePatterns(), includedFilenamePatterns()
    /**
     * @param string $dirPath
     * @param array $excludedDirPatterns & etc. - fnmatch() used
     * @return array[]
     */
    private static function listRecursive(array &$filesList, string $dirPath, FilesParams $params): void
    {
        $files = [];
        $dirs = [];

        foreach (array_filter(glob("$dirPath/*"), 'is_file') as $file) {
            $includeFile = false;

            foreach ($params->includedFilenamePatterns() as $filenamePattern) {
                if (fnmatch($filenamePattern, $file)) {
                    $includeFile = true;
                }
            }

            if (count($params->includedFilenamePatterns()) > 0 && !$includeFile) {
                continue;
            }

            $files[] = $file;
        }

        foreach (array_filter(glob("$dirPath/*"), 'is_dir') as $dir) {
            $excludeDir = false;
            foreach ($params->excludedDirPatterns() as $excludedDirPattern) {
                if (fnmatch($excludedDirPattern, $dir)) {
                    $excludeDir = true;
                }
            }

            if ($excludeDir) {
                continue;
            }

            $dirs[] = $dir;
        }

        $filesList = array_merge($filesList, $files);
        foreach ($dirs as $dir) {
            self::listRecursive($filesList, $dir, $params);
        }
    }

    public static function list(string $dirPath, FilesParams $params = null)
    {
        $dirPath = rtrim($dirPath, "/");

        $params = $params ?? new FilesParams();

        $filesList = [];

        self::listRecursive($filesList, $dirPath, $params);

        return $filesList;
    }
}
