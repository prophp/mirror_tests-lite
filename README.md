# ◦ Requirements

- PHP 8.0+
- Linux

# ◦ Install

```
composer require prophp/mirror_tests-lite --dev
```

```
vendor/bin/tests-lite
```

# ◦ Create test placeholder

```
bin/test create <location/path> <testTitle>[optional]
```

```
bin/test create src/Lib/Example missingFileException
```

# ◦ Run tests

```
bin/test
```

# ◦ Use inside tests

```
Helper::getDataDirPath($location)
```

# ◦ Custom config (optional)

`<rootPath/>config/tests-lite.json`

```
{
  "webrootPath": "/var/www/html/",
  "testsDirTitle": "tests"
}
```

